extends "res://addons/gut/test.gd"
# @see https://github.com/bitwes/Gut#creating_tests
var node = null;

func setup():
	var n = preload('res://tests/test-2d.tscn')
	node = n.instance()

func test_node_has_exit_button():
	var b = node.get_node('CenterContainer/exit')
	assert_true(b != null, 'CenterContainer/exit is not null')
	if (b):
		assert_true(b.get_class() == 'Button', 'CenterContainer/exit is a Button')
		var connected = gut.p(b.get_signal_connection_list('pressed'))
		assert_true(connected != [], 'CenterContainer/exit has connected signals')
		
