# Godot3 Template Project

A series of folders and files that I use when starting a Godot3-based project.

The `game` folder should have the engine.cfg and any rendered assets, scenes, etc.

Use the top-level `assets` folder for storing asset sources (eg. gimp .xcf), etc. that are
required.

# Using the template

1. Get a copy and remove existing history:

`
export PROJECT_NAME=project
git clone --recursive https://gitlab.com/kienan/godot3-project-template.git $PROJECT_NAME
cd $PROJECT_NAME
rm -rf .git ; git init .
`

2. Modify as needed.

# Copyright and license

Copyright 2017-2018 Kienan Stewart

See full copyright and license information the COPYRIGHT file.
